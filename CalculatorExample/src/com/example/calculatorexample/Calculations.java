package com.example.calculatorexample;

public class Calculations {

    private static final char CHAR = '@';
    private static final String STRING = "/*+-";
    static String symbols = STRING;
    static String input;

    public Calculations(String input) {
        Calculations.input = input;
    }

    public CharSequence getResult() {
        int count = getIterations(input);
        for (int i = 0; i < count; i++) {
            char firstSymbol = input.charAt(0);
            if (firstSymbol == '-') {
                StringBuilder modify = new StringBuilder(input);
                modify.setCharAt(0, CHAR);
                input = modify.toString();
            }
            char operator = getOperator(input);
            int index = input.indexOf(operator);
            String operand1 = getValues(index, 0);
            String operand2 = getValues(index, 1);
            System.out.println(count + " and Operator is " + operator + " and index is " + index + " and operand1 value is " + operand1 + " operand2 " + operand2);
            System.out.println(input);
            double result = getCalculations(operand1, operand2, operator);
            double error = 1123456789.0;
            if (result == error) {
                System.out.println("ERRORERRORERRORERRORERRORERRORERRORERROR");
                return "ERROR";
            }
            System.out.println("calculation result is " + result);
            input = input.replace(operand1 + operator + operand2, "" + result);
            System.out.println(">>>>>>>>>>> " + input + " >>>>>>>>> " + i);
        }
        if (input.equals("Infinity")) {
            return input;
        }
        try {
            double exception = Double.parseDouble(input);
        } catch (Exception e) {
            return "Error";
        }
        String precision = input.substring(input.indexOf('.') + 1, input.length());
        System.out.println("<<<<<<<<<< " + precision + " >>>>>" + input + ">>>> ");
        if (Long.parseLong(precision) == 0) {
            return input.substring(0, input.indexOf('.'));
        }
        return input;
    }

    private static double getCalculations(String operand1, String operand2, char operator) {
        float val1, val2;
        operand1 = operand1.replace("" + CHAR, "-");
        operand2 = operand2.replace("" + CHAR, "-");
        double result = 0;
        try {
            val1 = Float.parseFloat(operand1);
            val2 = Float.parseFloat(operand2);
            System.out.println("val1 is " + val1 + " and val2 is " + val2);
            switch (operator) {
                case '+' :
                    result = val1 + val2;
                    break;
                case '-' :
                    result = val1 - val2;
                    break;
                case '/' :
                    result = val1 / val2;
                    break;
                case '*' :
                    result = val1 * val2;
                    break;
            }
            System.out.println("final result is " + result);
        } catch (Exception e) {
            System.out.println("ERRORERRORERRORERRORERRORERRORERRORERROR>>2");
            return 1123456789.0;
        }
        return result;
    }

    private static boolean isnextOneOperator(String input, int index, int position) {
        boolean status = false;
        char chr;
        if (index + 1 > input.length()) {
            System.out.println("out of range :(");
            return status;
        }
        if (position == 1) {
            chr = input.charAt(index + 1);
        } else {
            chr = input.charAt(index - 1);
        }
        status = symbols.contains("" + chr);
        status = !status;
        return status;
    }

    private static String getValues(int index, int position) {
        String value = "0";
        int count = 0;
        int start = index + 1;
        System.out.println("1 index " + index + " position " + position);
        if (index + 1 == input.length() || index - 1 < 0) {
            System.out.println("out of range");
            return value;
        }
        boolean operatorStatus = isnextOneOperator(input, index, position);
        while (operatorStatus) {
            count++;
            if (position == 0) {
                index--;
            } else {
                index++;
            }
            if (index + 1 == input.length() || index - 1 < 0) {
                System.out.println("out of range");
                break;
            }
            System.out.println("2 index " + index + " position " + position + "and length is " + input.length());
            operatorStatus = isnextOneOperator(input, index, position);
        }
        if (position == 0) {
            start--;
            value = input.substring(start - count, start);
        } else {
            value = input.substring(start, start + count);
        }
        return value;
    }

    private static char getOperator(String input) {
        char Operator = 0;
        for (int i = 0; i < symbols.length(); i++) {
            if (input.contains("" + symbols.charAt(i))) {
                return symbols.charAt(i);
            }
        }
        return Operator;
    }

    private static int getIterations(String input) {
        int count = 0;
        for (int i = 1; i < input.length(); i++) {
            if (symbols.contains("" + input.charAt(i))) {
                count++;
            }
        }
        return count;
    }
}
