package com.example.calculatorexample;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.Button;
import android.widget.EditText;

public class CalculatorMainActivity extends Activity implements OnClickListener, OnLongClickListener {

    Button button_1, button_2, button_3, button_4, button_5, button_6, button_7, button_8, button_9, button_0, cancel;
    Button equql, add, sub, mul, div, point;
    EditText editor;
    boolean dotValidation = true;
    boolean symbolValidation = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator_main);
        getIds();
        button_0.setOnClickListener(this);
        button_1.setOnClickListener(this);
        button_2.setOnClickListener(this);
        button_3.setOnClickListener(this);
        button_4.setOnClickListener(this);
        button_5.setOnClickListener(this);
        button_6.setOnClickListener(this);
        button_7.setOnClickListener(this);
        button_8.setOnClickListener(this);
        button_9.setOnClickListener(this);
        equql.setOnClickListener(this);
        cancel.setOnClickListener(this);
        cancel.setOnLongClickListener(this);
        add.setOnClickListener(this);
        sub.setOnClickListener(this);
        mul.setOnClickListener(this);
        div.setOnClickListener(this);
        point.setOnClickListener(this);
    }

    public boolean onLongClick(View v) {
        switch (v.getId()) {
            case R.id.erase :
                editor.setText("");
                editor.append("");
                dotValidation = true;
                break;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.one :
                editor.append("1");
                symbolValidation = true;
                break;
            case R.id.two :
                editor.append("2");
                symbolValidation = true;
                break;
            case R.id.three :
                editor.append("3");
                symbolValidation = true;
                break;
            case R.id.four :
                editor.append("4");
                symbolValidation = true;
                break;
            case R.id.five :
                editor.append("5");
                symbolValidation = true;
                break;
            case R.id.six :
                editor.append("6");
                symbolValidation = true;
                break;
            case R.id.seven :
                editor.append("7");
                symbolValidation = true;
                break;
            case R.id.eight :
                editor.append("8");
                symbolValidation = true;
                break;
            case R.id.nine :
                editor.append("9");
                symbolValidation = true;
                break;
            case R.id.zero :
                editor.append("0");
                symbolValidation = true;
                break;
            case R.id.point :
                if (dotValidation && editor.getText().toString().length() > 0) {
                    editor.append(".");
                    dotValidation = false;
                }
                break;
            case R.id.addition :
                if (symbolValidation) {
                    editor.append("+");
                    dotValidation = true;
                    symbolValidation = false;
                }
                break;
            case R.id.mul :
                if (symbolValidation) {
                    editor.append("*");
                    dotValidation = true;
                    symbolValidation = false;
                }
                break;
            case R.id.substract :
                if (symbolValidation) {
                    editor.append("-");
                    dotValidation = true;
                    symbolValidation = false;
                }
                break;
            case R.id.div :
                if (symbolValidation) {
                    editor.append("/");
                    dotValidation = true;
                    symbolValidation = false;
                }
                break;
            case R.id.erase :
                String trim = editor.getText().toString();
                if (trim.length() > 0) {
                    trim = trim.substring(0, trim.length() - 1);
                }
                editor.setText("");
                editor.append(trim);
                break;
            case R.id.equal :
            	if(editor.getText().toString().length()>0){
                System.out.println("in equal methode");
                Calculations calculations = new Calculations(editor.getText().toString());
                editor.setText("");
                editor.append(calculations.getResult());
            	}
                break;
        }
    }

    private void getIds() {
        button_0 = (Button) findViewById(R.id.zero);
        button_1 = (Button) findViewById(R.id.one);
        button_2 = (Button) findViewById(R.id.two);
        button_3 = (Button) findViewById(R.id.three);
        button_4 = (Button) findViewById(R.id.four);
        button_5 = (Button) findViewById(R.id.five);
        button_6 = (Button) findViewById(R.id.six);
        button_7 = (Button) findViewById(R.id.seven);
        button_8 = (Button) findViewById(R.id.eight);
        button_9 = (Button) findViewById(R.id.nine);
        cancel = (Button) findViewById(R.id.erase);
        equql = (Button) findViewById(R.id.equal);
        editor = (EditText) findViewById(R.id.editText1);
        add = (Button) findViewById(R.id.addition);
        sub = (Button) findViewById(R.id.substract);
        mul = (Button) findViewById(R.id.mul);
        div = (Button) findViewById(R.id.div);
        point = (Button) findViewById(R.id.point);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
